YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [],
    "modules": [
        "RecordHelper"
    ],
    "allModules": [
        {
            "displayName": "RecordHelper",
            "name": "RecordHelper",
            "description": "Helper to use when dealing with records on Netsuite."
        }
    ],
    "elements": []
} };
});