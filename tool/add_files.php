<?php

require_once './PHPToolkit/NetSuiteService.php';

$service = new NetSuiteService();

// read configuration file
$lines = file("conf.txt");

foreach($lines as $line) {
	$data = split("->", $line); 
	// var_dump($data);
	 
	uploadFile(trim($data[1]), $data[0], $service);
} 

function uploadFile($fpath, $folderid, $service){

	$f = split("/", $fpath);
	$filename = $f[count($f) - 1];

	// create Task record
	$file = new File();
	$file->name = $filename;
	$file->atachFrom = FileAttachFrom::_computer;
	$content = file_get_contents($fpath);
	$file->content = $content;
	$folder = new RecordRef();
	$folder->internalId = $folderid;
	$file->folder = $folder;

	$request = new AddRequest();
	$request->record = $file;

	$addResponse = $service->add($request);
	if (!$addResponse->writeResponse->status->isSuccess) {
	    var_dump($addResponse);
	} else {
	    echo "ADD SUCCESS, id " . $addResponse->writeResponse->baseRef->internalId;
	}
}
?> 

