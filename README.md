# uploadify

## Overview

Uploadify is a tool to improve the process of uploading files into file cabinet in Netsuite.

The user will be able to configure which files upload and in which folder, for example you can upload suitescript files directly from your computer, javascript files to use on your site and more.

## Prerequisites
For your computer
* PHP version 5.+
* NodeJS
* Gulp

In your Netsuite account
* You must have the web service enable.

## Installation


## How to use it?

First configure the NSConfig.php file to provide details about your account to access the NS web services.

Second configure the conf.txt file to provide details about which files you want to upload.


To use access the folder and run the command "gulp upload".
